package hz.hafidz.favoritecatalogue;

import android.provider.BaseColumns;

public class TblFavorite implements BaseColumns {

    public static String FAVORITE = "favorite";
    public static String ID = "_id";
    public static String ID_MOVIE = "movieId";

}
