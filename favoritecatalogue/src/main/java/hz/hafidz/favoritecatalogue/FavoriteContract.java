package hz.hafidz.favoritecatalogue;

import android.content.UriMatcher;
import android.net.Uri;

public class FavoriteContract {
    public static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    private static final String AUTHORITY = "hz.hafidz.moviecatalogue";
    private static final String BASE_PATH = TblFavorite.FAVORITE;
    public static final Uri CONTENT_URI = new Uri.Builder().scheme("content")
            .authority(AUTHORITY)
            .appendPath(BASE_PATH).build();
    private static final int GET_MOVIES = 1;
    private static final int GET_MOVIES_BY_id = 2;

    static {
        uriMatcher.addURI(AUTHORITY, BASE_PATH, GET_MOVIES);
        uriMatcher.addURI(AUTHORITY, BASE_PATH + "/#", GET_MOVIES_BY_id);
    }
}
