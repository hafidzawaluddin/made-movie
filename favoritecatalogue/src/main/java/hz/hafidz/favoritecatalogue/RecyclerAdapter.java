package hz.hafidz.favoritecatalogue;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private ArrayList<FavoriteModel> mModels;

    RecyclerAdapter(ArrayList<FavoriteModel> mModels) {
        this.mModels = mModels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        FavoriteModel model = mModels.get(i);
        viewHolder.textView.setText(model.getTitle());
        Picasso.get().load(model.getUrl()).into(viewHolder.imgBannenr);
    }

    @Override
    public int getItemCount() {
        if (mModels != null) {
            return mModels.size();
        } else {
            return 0;
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView textView;
        ImageView imgBannenr;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.txtId);
            imgBannenr = itemView.findViewById(R.id.imgBanner);
        }
    }
}
