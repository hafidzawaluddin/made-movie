package hz.hafidz.favoritecatalogue;

public class FavoriteModel {
    private int _id;
    private int idMovie;
    private String title;
    private String url;

    public FavoriteModel(int _id, int idMovie, String title, String url) {
        this._id = _id;
        this.idMovie = idMovie;
        this.title = title;
        this.url = url;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public int getIdMovie() {
        return idMovie;
    }

    public void setIdMovie(int idMovie) {
        this.idMovie = idMovie;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
