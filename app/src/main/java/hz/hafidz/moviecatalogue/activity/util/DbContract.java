package hz.hafidz.moviecatalogue.activity.util;

import android.database.Cursor;
import android.net.Uri;

public class DbContract {
    public static String FAVORITE = "favorite";
    public static String ID = "_id";
    public static String ID_MOVIE = "movieId";
    public static String TITLE = "title";
    public static String URL = "url";

    private static final String AUTHORITY = "hz.hafidz.moviecatalogue";
    public static final Uri CONTENT_URI = new Uri.Builder().scheme("content")
            .authority(AUTHORITY)
            .appendPath(FAVORITE)
            .build();

    public static String getColumnString(Cursor cursor, String columnName) {
        return cursor.getString( cursor.getColumnIndex(columnName) );
    }
    public static int getColumnInt(Cursor cursor, String columnName) {
        return cursor.getInt( cursor.getColumnIndex(columnName) );
    }
    public static long getColumnLong(Cursor cursor, String columnName) {
        return cursor.getLong( cursor.getColumnIndex(columnName) );
    }
}
