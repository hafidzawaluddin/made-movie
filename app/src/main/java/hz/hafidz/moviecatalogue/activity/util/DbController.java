package hz.hafidz.moviecatalogue.activity.util;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class DbController {

    private SQLiteDatabase mDatabase;
    private String query;

    public DbController(Context context) {
        DbHelper mHelper = new DbHelper(context);
        mDatabase = mHelper.getWritableDatabase();
    }

    public void insertData(String idMovie, String title, String url) {
        try {
            query = "INSERT INTO " + DbContract.FAVORITE + " VALUES(NULL ,'" + idMovie + "', " +
                    "'" + title + "', '" + url + "')";
            System.out.println(query);
            mDatabase.execSQL(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteData(String idMovie) {
        try {
            query = "DELETE FROM " + DbContract.FAVORITE + " WHERE " + DbContract.ID_MOVIE + " = '" + idMovie + "'";
            System.out.println(query);
            mDatabase.execSQL(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean getData(String idMovie) {
        try {
            query = "SELECT * FROM " + DbContract.FAVORITE + " WHERE " + DbContract.ID_MOVIE + " = '" + idMovie + "'";
            System.out.println(query);
            Cursor c = mDatabase.rawQuery(query, null);
            if (c.getCount() != 0) {
                c.close();
                return true;
            } else {
                c.close();
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public List<FavoriteModel> getAllData() {
        try {
            List<FavoriteModel> mModels = new ArrayList<>();
            query = "SELECT * FROM " + DbContract.FAVORITE;
            System.out.println(query);
            Cursor c = mDatabase.rawQuery(query, null);
            c.moveToFirst();
            do {
                mModels.add(new FavoriteModel(c.getString(1), c.getString(2), c.getString(3)));
            } while (c.moveToNext());
            c.close();
            return mModels;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
