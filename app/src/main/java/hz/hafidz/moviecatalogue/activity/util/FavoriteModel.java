package hz.hafidz.moviecatalogue.activity.util;

public class FavoriteModel {
    private String idMovie;
    private String title;
    private String url;

    public FavoriteModel(String idMovie, String title, String url) {
        this.idMovie = idMovie;
        this.title = title;
        this.url = url;
    }

    public String getIdMovie() {
        return idMovie;
    }

    public void setIdMovie(String idMovie) {
        this.idMovie = idMovie;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
