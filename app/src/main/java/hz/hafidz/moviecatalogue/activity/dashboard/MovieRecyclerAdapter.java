package hz.hafidz.moviecatalogue.activity.dashboard;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import hz.hafidz.moviecatalogue.R;
import hz.hafidz.moviecatalogue.activity.detail.DetailActivity;
import hz.hafidz.moviecatalogue.network.dao.ResultsItem;
import hz.hafidz.moviecatalogue.network.util.RetrofitClient;

public class MovieRecyclerAdapter extends RecyclerView.Adapter<MovieRecyclerAdapter.ViewHolder> {

    private List<ResultsItem> mModel;
    private Context mContext;

    public MovieRecyclerAdapter(List<ResultsItem> mModel, Context mContext) {
        this.mModel = mModel;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.list_move_recycler_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ResultsItem model = mModel.get(position);
        Picasso.get().load(RetrofitClient.IMAGE_URL+model.getPosterPath()).into(holder.imgPoster);
        holder.txtTitle.setText(model.getTitle());
        holder.btnDetail.setOnClickListener(v -> {
            Intent i = new Intent(mContext, DetailActivity.class);
            i.putExtra("id", model.getId()+"");
            mContext.startActivity(i);
        });
    }

    public void setmModel(List<ResultsItem> model){
        this.mModel = model;
    }

    public List<ResultsItem> getList(){
        return mModel;
    }

    @Override
    public int getItemCount() {
        return mModel.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgPoster;
        TextView txtTitle;
        Button btnDetail, btnShare;
        ViewHolder(View itemView) {
            super(itemView);
            imgPoster = itemView.findViewById(R.id.imgPoster);
            txtTitle = itemView.findViewById(R.id.txtTitle);
            btnDetail = itemView.findViewById(R.id.btnDetail);
            btnShare = itemView.findViewById(R.id.btnShare);
        }
    }

}
