package hz.hafidz.moviecatalogue.activity.util;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class FavoriteProvider extends ContentProvider {

    public static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    private static final String AUTHORITY = "hz.hafidz.moviecatalogue";
    private static final String BASE_PATH =  DbContract.FAVORITE;
    public static final Uri CONTENT_URI = new Uri.Builder().scheme("content")
            .authority(AUTHORITY)
            .appendPath(BASE_PATH)
            .build();
    private static final int GET_MOVIES = 1;
    private static final int GET_MOVIES_BY_id = 2;

    static {
        uriMatcher.addURI(AUTHORITY, BASE_PATH, GET_MOVIES);
        uriMatcher.addURI(AUTHORITY, BASE_PATH + "/#", GET_MOVIES_BY_id);
    }

    private SQLiteDatabase mDatabase;

    @Override
    public boolean onCreate() {
        DbHelper mHelper = new DbHelper(getContext());
        mDatabase = mHelper.getWritableDatabase();
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        Cursor cursor = null;
        if (uriMatcher.match(uri) == GET_MOVIES) {
            cursor = mDatabase.query(DbContract.FAVORITE, projection, selection, selectionArgs,
                    null, null, sortOrder == null ? DbContract.ID+ " DESC" : sortOrder);
        }
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        long id = mDatabase.insert(DbContract.FAVORITE, null, values);

        if (id > 0) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
        throw new SQLException("Insertion Failed for URI :" + uri);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        int count;
        switch (uriMatcher.match(uri)) {
            case GET_MOVIES:
                count = mDatabase.delete(DbContract.FAVORITE, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("This is an Unknown URI " + uri);
        }
        return count;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        int count;
        switch (uriMatcher.match(uri)) {
            case GET_MOVIES:
                count = mDatabase.update(DbContract.FAVORITE, values, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("This is an Unknown URI " + uri);
        }
        return count;
    }
}
