package hz.hafidz.moviecatalogue.activity.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import hz.hafidz.moviecatalogue.R;
import hz.hafidz.moviecatalogue.activity.dashboard.fragment.nowplaying.NowPlayingFragment;
import hz.hafidz.moviecatalogue.activity.dashboard.fragment.upcoming.UpcomingFragment;
import hz.hafidz.moviecatalogue.activity.search.MainActivity;
import hz.hafidz.moviecatalogue.notification.NotifManager;

public class DashboardActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private NotifManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        setTabLayout();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        manager = new NotifManager(this);
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                startActivity(new Intent(this, MainActivity.class));
                break;

            case R.id.action_alarm:
                setAlarm();
                break;

            default:
                Intent mIntent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
                startActivity(mIntent);
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
//        int id = item.getItemId();
//
//        if (id == R.id.nav_camera) {
//            // Handle the camera action
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//  
//        } else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setTabLayout() {
        TabLayout mTabLayout = findViewById(R.id.movieTab);
        ViewPager mViewPager = findViewById(R.id.movieViewPager);
        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        FragmentAdapter mAdapter = new FragmentAdapter(getSupportFragmentManager());
        mAdapter.addFragment(new UpcomingFragment(), getResources().getString(R.string.upcoming));
        mAdapter.addFragment(new NowPlayingFragment(), getResources().getString(R.string.nowplaying));
        mViewPager.setAdapter(mAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    private void setAlarm() {
        if (manager.isTurnOn()) {
            Toast.makeText(this, "Alarm Mati", Toast.LENGTH_SHORT).show();
            manager.turnOff(NotifManager.DAILY);
            manager.turnOff(NotifManager.RELEASE_TODAY);
        } else {
            Toast.makeText(this, "Alarm Aktif", Toast.LENGTH_SHORT).show();
            manager.turnOn(7, 0, 0, NotifManager.DAILY, null);
            manager.turnOn(8, 0, 0, NotifManager.RELEASE_TODAY, null);
        }
    }
}
