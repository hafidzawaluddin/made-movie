package hz.hafidz.moviecatalogue.activity.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "favorite";
    private static final int DATABASE_VERSION = 7;

    private static final String CREATE_TABLE_FAVORITE = "CREATE TABLE " +
            DbContract.FAVORITE + "(" + DbContract.ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            " " + DbContract.ID_MOVIE + " TEXT NOT NULL UNIQUE, " + DbContract.TITLE + " TEXT NOT NULL," +
            "  " + DbContract.URL + " TEXT NOT NULL)";

    private static final String DELETE_TABLE_FAVORITE = "DROP TABLE IF EXISTS " + DbContract.FAVORITE;

    DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        System.out.println(CREATE_TABLE_FAVORITE);
        db.execSQL(CREATE_TABLE_FAVORITE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DELETE_TABLE_FAVORITE);
        onCreate(db);
    }
}
