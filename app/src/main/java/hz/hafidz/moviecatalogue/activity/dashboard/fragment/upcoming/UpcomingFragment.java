package hz.hafidz.moviecatalogue.activity.dashboard.fragment.upcoming;

import android.content.Context;
import android.graphics.Movie;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.nio.channels.ShutdownChannelGroupException;
import java.util.ArrayList;
import java.util.List;

import hz.hafidz.moviecatalogue.R;
import hz.hafidz.moviecatalogue.activity.dashboard.MovieRecyclerAdapter;
import hz.hafidz.moviecatalogue.network.dao.Response;
import hz.hafidz.moviecatalogue.network.dao.ResultsItem;
import hz.hafidz.moviecatalogue.network.util.RetrofitClient;
import hz.hafidz.moviecatalogue.network.util.RetrofitService;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class UpcomingFragment extends Fragment {

    private MovieRecyclerAdapter mAdapter;
    private RecyclerView upComingRecycler;
    private ArrayList<ResultsItem> mModel = new ArrayList<>();

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        upComingRecycler = view.findViewById(R.id.upcomingRecycler);
        setRecycler();
        if(savedInstanceState!= null){
            ArrayList<ResultsItem> list = savedInstanceState.getParcelableArrayList("list");
            mAdapter.setmModel(list);
            mAdapter.notifyDataSetChanged();
            upComingRecycler.setAdapter(mAdapter);
        }else {
            getData();
        }
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.upcoming_fragment_layout, container, false);
    }

    private void setRecycler() {
        mAdapter = new MovieRecyclerAdapter(mModel, getActivity());
        upComingRecycler.setAdapter(mAdapter);
        upComingRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void getData(){
        RetrofitService mService = RetrofitClient.mClient();
        mService.getUpComing(RetrofitClient.TOKEN, "en-US")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<Response>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response response) {
                        mModel.addAll(response.getResults());
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        mAdapter.notifyDataSetChanged();
                    }
                });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("list",new ArrayList<>(mAdapter.getList()));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState!= null){
            ArrayList<ResultsItem> list = savedInstanceState.getParcelableArrayList("list");
            mAdapter.setmModel(list);
            mAdapter.notifyDataSetChanged();
            upComingRecycler.setAdapter(mAdapter);
        }
    }
}
