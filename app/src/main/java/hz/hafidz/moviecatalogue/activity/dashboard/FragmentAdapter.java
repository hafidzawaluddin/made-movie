package hz.hafidz.moviecatalogue.activity.dashboard;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

public class FragmentAdapter extends FragmentPagerAdapter {

    private ArrayList<Fragment> mFragmentItems = new ArrayList<>();
    private ArrayList<String> mFragmentTitles = new ArrayList<>();

    FragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    void addFragment(Fragment fragmentItem, String fragmentTitle) {
        mFragmentItems.add(fragmentItem);
        mFragmentTitles.add(fragmentTitle);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentItems.get(position);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitles.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentItems.size();
    }
}
