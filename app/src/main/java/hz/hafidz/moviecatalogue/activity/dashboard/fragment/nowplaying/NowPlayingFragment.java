package hz.hafidz.moviecatalogue.activity.dashboard.fragment.nowplaying;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import hz.hafidz.moviecatalogue.R;
import hz.hafidz.moviecatalogue.activity.dashboard.MovieRecyclerAdapter;
import hz.hafidz.moviecatalogue.network.dao.Response;
import hz.hafidz.moviecatalogue.network.dao.ResultsItem;
import hz.hafidz.moviecatalogue.network.util.RetrofitClient;
import hz.hafidz.moviecatalogue.network.util.RetrofitService;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class NowPlayingFragment extends Fragment {

    private MovieRecyclerAdapter mAdapter;
    private RecyclerView nowPlayingRecycler;
    private List<ResultsItem> mModel = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.upcoming_fragment_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        nowPlayingRecycler = view.findViewById(R.id.upcomingRecycler);
        setRecycler();
        if(savedInstanceState!= null){
            ArrayList<ResultsItem> list = savedInstanceState.getParcelableArrayList("list");
            mAdapter.setmModel(list);
            mAdapter.notifyDataSetChanged();
            nowPlayingRecycler.setAdapter(mAdapter);
        }else {
            getData();
        }
        super.onViewCreated(view, savedInstanceState);
    }

    private void setRecycler() {
        mAdapter = new MovieRecyclerAdapter(mModel, getActivity());
        nowPlayingRecycler.setAdapter(mAdapter);
        nowPlayingRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void getData(){
        RetrofitService mService = RetrofitClient.mClient();
        mService.getNowPlaying(RetrofitClient.TOKEN, "en-US")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<Response>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response response) {
                        mModel.addAll(response.getResults());
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        mAdapter.notifyDataSetChanged();
                    }
                });

    }

    private void hideLoading(){
        nowPlayingRecycler.setVisibility(View.GONE);
    }

    private void showLoading(){
        nowPlayingRecycler.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("list",new ArrayList<>(mAdapter.getList()));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState!= null){
            ArrayList<ResultsItem> list = savedInstanceState.getParcelableArrayList("list");
            mAdapter.setmModel(list);
            mAdapter.notifyDataSetChanged();
            nowPlayingRecycler.setAdapter(mAdapter);
        }
    }
}
