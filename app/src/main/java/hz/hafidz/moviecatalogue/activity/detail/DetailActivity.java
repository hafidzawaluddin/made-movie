package hz.hafidz.moviecatalogue.activity.detail;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import hz.hafidz.moviecatalogue.R;
import hz.hafidz.moviecatalogue.activity.util.DbController;
import hz.hafidz.moviecatalogue.network.dao.DetailDao;
import hz.hafidz.moviecatalogue.network.util.RetrofitClient;
import hz.hafidz.moviecatalogue.network.util.RetrofitService;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DetailActivity extends AppCompatActivity {

    private TextView txtTitle, txtDate, txtDesc;
    private ImageView imgPoster;
    private DbController mController;
    private Button btnFav;
    private String id;
    private Boolean fav = false;
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        init();
        onCLick();
    }

    private void getApi(String id) {
        RetrofitService mService = RetrofitClient.mClient();
        mService.getDetail(id, RetrofitClient.TOKEN, "en-US")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<DetailDao>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(DetailDao detailDao) {
                        Picasso.get().load(RetrofitClient.IMAGE_URL + detailDao.getPosterPath()).into(imgPoster);
                        txtTitle.setText(detailDao.getTitle());
                        txtDate.setText(detailDao.getReleaseDate());
                        txtDesc.setText(detailDao.getOverview());
                        url = RetrofitClient.IMAGE_URL + detailDao.getPosterPath();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void init() {
        id = getIntent().getStringExtra("id");

        txtTitle = findViewById(R.id.txtTitle);
        txtDate = findViewById(R.id.txtDate);
        txtDesc = findViewById(R.id.txtDesc);
        imgPoster = findViewById(R.id.imgPoster);
        btnFav = findViewById(R.id.btnFav);
        mController = new DbController(this);
        getApi(id);
        fav = mController.getData(id);

        if (fav) {
            btnFav.setText(getString(R.string.unfavorite));
        }
    }

    private void onCLick() {
        btnFav.setOnClickListener(v -> {
            if (fav) {
                mController.deleteData(id);
                btnFav.setText(getText(R.string.favorite));
                fav = false;
            } else {
                mController.insertData(id, txtTitle.getText().toString(), url);
                btnFav.setText(getText(R.string.unfavorite));
                fav = true;

            }
        });
    }


}
