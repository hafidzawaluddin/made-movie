package hz.hafidz.moviecatalogue.activity.search;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import hz.hafidz.moviecatalogue.R;
import hz.hafidz.moviecatalogue.activity.detail.DetailActivity;
import hz.hafidz.moviecatalogue.network.dao.ResultsItem;
import hz.hafidz.moviecatalogue.network.util.RetrofitClient;

public class SearchRecyclerAdapter extends RecyclerView.Adapter<SearchRecyclerAdapter.ViewHolder> {

    private List<ResultsItem> mModel;
    private Context mContext;

    SearchRecyclerAdapter(List<ResultsItem> mModel, Context mContext) {
        this.mModel = mModel;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ResultsItem model = mModel.get(position);
        Picasso.get().load(RetrofitClient.IMAGE_URL + model.getPosterPath()).into(holder.img);
        holder.txt.setText(model.getTitle());
        holder.itemView.setOnClickListener((v) -> {
            Intent i = new Intent(mContext, DetailActivity.class);
            i.putExtra("id", model.getId()+"");
            mContext.startActivity(i);
        });

    }

    @Override
    public int getItemCount() {
        return mModel.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView txt;

        ViewHolder(View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.imgPoster);
            txt = itemView.findViewById(R.id.txtTitle);
        }
    }

}