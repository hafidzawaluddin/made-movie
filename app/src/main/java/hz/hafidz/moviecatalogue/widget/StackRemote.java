package hz.hafidz.moviecatalogue.widget;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;

import java.util.List;

import hz.hafidz.moviecatalogue.R;
import hz.hafidz.moviecatalogue.activity.util.DbController;
import hz.hafidz.moviecatalogue.activity.util.FavoriteModel;

public class StackRemote implements RemoteViewsService.RemoteViewsFactory {

    private List<FavoriteModel> mModels;
    private Context mContext;
    private DbController mController;

    StackRemote(Context mContext, Intent intent) {
        this.mContext = mContext;
        int mAppWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        mController = new DbController(mContext);
    }

    @Override
    public void onCreate() {
        mModels = mController.getAllData();
    }

    @Override
    public void onDataSetChanged() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public int getCount() {
        Log.wtf("getCount: ",""+ mModels.size());
        return mModels.size();
    }

    @Override
    public RemoteViews getViewAt(int position) {
        RemoteViews remoteViews = new RemoteViews(mContext.getPackageName(), R.layout.widget_recycler_item);
        FavoriteModel model = mModels.get(position);
        Bundle bundle = new Bundle();
        bundle.putInt(MovieWidget.EXTRA_ITEM, position);
        Intent intent = new Intent();
        intent.putExtras(bundle);
        Bitmap bitmap = null;

        try {
            bitmap = Glide.with(mContext)
                    .asBitmap()
                    .load(model.getUrl())
                    .into(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                    .get();
        } catch (Exception e) {
            e.printStackTrace();
        }

        remoteViews.setImageViewBitmap(R.id.imgThumbnail, bitmap);
        return remoteViews;
    }



    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

}
