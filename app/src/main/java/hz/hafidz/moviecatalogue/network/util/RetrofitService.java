package hz.hafidz.moviecatalogue.network.util;

import hz.hafidz.moviecatalogue.network.dao.DetailDao;
import hz.hafidz.moviecatalogue.network.dao.Response;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RetrofitService {

    @GET("search/movie")
    Observable<Response> getMovie(@Query("page") String page,
                                  @Query("query") String name,
                                  @Query("language") String language,
                                  @Query("api_key") String key);

    @GET("movie/{id}")
    Observable<DetailDao> getDetail(@Path("id") String id,
                                    @Query("api_key") String key,
                                    @Query("language") String language);

    @GET("movie/now_playing")
    Observable<Response> getNowPlaying(@Query("api_key") String key,
                                       @Query("language") String language);

    @GET("movie/upcoming")
    Observable<Response> getUpComing(@Query("api_key") String key,
                                     @Query("language") String language);
}
