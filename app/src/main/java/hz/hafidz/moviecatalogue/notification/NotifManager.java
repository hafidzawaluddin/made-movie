package hz.hafidz.moviecatalogue.notification;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import java.util.Calendar;

public class NotifManager {
    public static final int DAILY = 100;
    public static final int RELEASE_TODAY = 101;
    public static final String REQUEST_CODE = "request_code";

    private Context context;
    private AlarmManager alarmManager;
    private NotifPreferences appPreferences;

    public NotifManager(Context context) {
        this.context = context;
        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        appPreferences = new NotifPreferences(context);
    }

    public void turnOff(int code) {
        Intent intent = new Intent(context, NotifyNewMovie.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, code, intent, 0);
        alarmManager.cancel(pendingIntent);
        if (code == DAILY) {
            appPreferences.setDailySchedule(0);
        } else if (code == RELEASE_TODAY) {
            appPreferences.setReleaseTodaySchedule(0);
        }
    }

    public boolean isTurnOn(){
        return appPreferences.isReleaseTodayScheduled() && appPreferences.isDailyScheduled();
    }

    public void turnOn(int hour, int min, int sec, int code, Bundle bundle) {
        Intent intent = new Intent(context, NotifyNewMovie.class);
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putInt(REQUEST_CODE, code);
        intent.putExtras(bundle);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, code, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH), hour, min, sec);
        alarmManager.setInexactRepeating(AlarmManager.RTC, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        if (code == DAILY) {
            appPreferences.setDailySchedule(calendar.getTimeInMillis());
        } else if (code == RELEASE_TODAY) {
            appPreferences.setReleaseTodaySchedule(calendar.getTimeInMillis());
        }
    }

}
