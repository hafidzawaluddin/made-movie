package hz.hafidz.moviecatalogue.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import hz.hafidz.moviecatalogue.R;
import hz.hafidz.moviecatalogue.activity.detail.DetailActivity;
import hz.hafidz.moviecatalogue.activity.search.MainActivity;
import hz.hafidz.moviecatalogue.network.dao.Response;
import hz.hafidz.moviecatalogue.network.dao.ResultsItem;
import hz.hafidz.moviecatalogue.network.util.DateFormatter;
import hz.hafidz.moviecatalogue.network.util.RetrofitClient;
import hz.hafidz.moviecatalogue.network.util.RetrofitService;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class NotifyNewMovie extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            if (bundle.getInt(NotifManager.REQUEST_CODE) == NotifManager.DAILY) {
                showNotif(context, "Hei", "Kembali Kesini", 1, null, MainActivity.class);
            } else if (bundle.getInt(NotifManager.REQUEST_CODE) == NotifManager.RELEASE_TODAY) {
                getRealease(context);
            }
        }
    }

    private void showNotif(Context context, String title, String message, int notifId, Bundle bundle, Class target) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_menu_camera)
                .setContentTitle(title)
                .setContentText(message)
                .setColor(ContextCompat.getColor(context, R.color.colorAccent))
                .setSound(alarmSound);
        Intent i = new Intent(context, target);
        if (bundle == null) {
            bundle = new Bundle();
        }
        i.putExtras(bundle);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, notifId, i, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);
        notificationManager.notify(notifId, builder.build());
    }

    private void getRealease(Context context) {
        RetrofitService mService = RetrofitClient.mClient();
        mService.getNowPlaying(RetrofitClient.TOKEN, "en-US")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<Response>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response response) {
                        Calendar now = Calendar.getInstance();
                        for (int i = 0; i < response.getResults().size(); i++) {
                            ResultsItem item = response.getResults().get(i);
                            try {
                                Calendar releaseDate = DateFormatter.format("yyyy-MM-dd").getCalendar(item.getRealeaseDate());
                                if (now.get(Calendar.YEAR) == releaseDate.get(Calendar.YEAR) &&
                                        now.get(Calendar.MONTH) == releaseDate.get(Calendar.MONTH) &&
                                        now.get(Calendar.DAY_OF_MONTH) == releaseDate.get(Calendar.DAY_OF_MONTH)) {
                                    Bundle bundle = new Bundle();
                                    bundle.putParcelable("movie", item);
                                    showNotif(context, item.getTitle(),
                                            "Hei Ada yang baru Lho", i + 2, bundle, DetailActivity.class);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
