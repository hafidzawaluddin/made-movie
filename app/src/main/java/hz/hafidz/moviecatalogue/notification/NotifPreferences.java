package hz.hafidz.moviecatalogue.notification;

import android.content.Context;
import android.content.SharedPreferences;

public class NotifPreferences {
    public static final String REALEASE = "release_today";
    public static final String DAILY = "daily";
    private String PREF_NAME = "moviepref";

    private Context mContext;
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;

    public NotifPreferences(Context mContext) {
        this.mContext = mContext;
        mSharedPreferences = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
    }

    private void put(String key, String value) {
        mEditor.putString(key, value);
        mEditor.apply();
    }

    private String get(String key) {
        return mSharedPreferences.getString(key, null);
    }

    public void setDailySchedule(long timestamp) {
        put(DAILY, timestamp == 0 ? null : String.valueOf(timestamp));
    }

    public void setReleaseTodaySchedule(long timestamp) {
        put(REALEASE, timestamp == 0 ? null : String.valueOf(timestamp));
    }

    public boolean isReleaseTodayScheduled() {
        return get(REALEASE) != null;
    }

    public boolean isDailyScheduled() {
        return get(DAILY) != null;
    }
}
